# aliases: Build redirection pages for page aliases.

## Documentation

A page can define 'aliases=[...]' to generate pages in those locations that
redirect to the page.

[Back to reference index](../README.md)
